.PHONY: all clean
all: process_list generator simulation

process_list: src/process_list.h src/process_list.c
	gcc -g -c -o src/process_list.o src/process_list.c

simulation: tests/simulation.c process_list
	gcc -o simulation tests/simulation.c src/process_list.o

generator: tests/generator.c
	gcc -o generator tests/generator.c

clean:
	rm simulation
	rm src/process_list.o
	rm generator
