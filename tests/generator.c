/**
 *  This program generates a file of random processes for simulation.
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// Macro for generating a random timeslice in the range {10, 20, 30, ... , 200}
#define GENERATE_COMPLETION_TIME ((rand() % 20) + 1) * 10

int idx = 0;

void generate(int quantity, int tickets)
{
    while(quantity-- > 0)
    {
        printf("%d, %d, %d\n", idx++, GENERATE_COMPLETION_TIME, tickets);
    }
}

int main()
{
    srand(time(NULL));
    generate(25, 20);
    generate(25, 10);
    generate(25, 5);
    generate(25, 1);
}
