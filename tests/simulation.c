#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "../src/process_list.h"

#define TIMESLICE 20 // 20msec per lottery
#define OVERHEAD 1 // 1msec per context switch

int count_tickets(process_t* root)
{
    int count = 0;
    while(root != NULL)
    {
        count += root->tickets;
        root = root->next;
    }
    return count;
}

process_t* find_winner(process_t* root, int winner)
{
    while( root != NULL )
    {
        winner -= root->tickets;
        if( winner <= 0 ) break;
        root = root->next;
    }
    return root;
}

int main(int argc, char** argv)
{
    srand(time(NULL));

    int t = 0;
    int class_times[4] ={ 0, 0, 0, 0 };
    process_t* root = read_file(argv[1]);

    while(traverse(root) != 0)
    {
        int c = count_tickets(root);
        int r = rand() % c;
        process_t* winner = find_winner(root, r);
        if(winner != NULL)
        {
            winner->quanta -= TIMESLICE;
            t += TIMESLICE + OVERHEAD;
            if( winner->quanta <= 0 )
            {
#ifdef DEBUG
                printf("Removing pid_%d after %dmsec\n", winner->pid, t);
#endif
                class_times[winner->pid / 25] += t;
                remove_from_list(&root, winner);
            }
        }
    }

    // Print results
    printf("Total Time Taken: %dmsec\n", t);
    printf("\tReal Time\t(20 tickets)\tavg: %fmsec\n", ((double)class_times[0])/25 );
    printf("\tSystem\t\t(10 tickets)\tavg: %fmsec\n", ((double)class_times[1])/25 );
    printf("\tUser\t\t(5 tickets)\tavg: %fmsec\n", ((double)class_times[2])/25 );
    printf("\tBackground\t(1 ticket)\tavg: %fmsec\n", ((double)class_times[3])/25 );
}
