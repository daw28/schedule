/**
 * @file process_list.c
 * @author David Watson
 * @date 3 Aug 2017
 * @brief An implementation of the process linked list defined in process_list.h
 *
 * A linked list of processes which each item containing processID, quanta and
 * number of lottery tickets as feilds. Each funtion accepts a pointer to the
 * root node of the list plus any other parameters the functions requires.
 * Standard linked list functions have been implemented: insert, delete,
 * traverse and find.
 */
#include <stdlib.h> // for malloc/free
#include <stdio.h>  // for reading csv files
#include <string.h> // for parsing csv files (strtok)
#include "process_list.h"

#define nullptr 0

process_t* insert( process_t* root, int _pid, int _quanta, int _tickets )
{
    process_t* newProcess = (process_t*)malloc( sizeof(process_t) );
    newProcess->pid = _pid;
    newProcess->quanta = _quanta;
    newProcess->tickets = _tickets;
    newProcess->next = root;
    return newProcess;
}

void remove_from_list( process_t** root, process_t* item )
{
    process_t* tmp;
    if(*root == item)
    {
        *root = item->next;
        free(item);
        return;
    }

    tmp = *root;
    while( tmp->next != item ) tmp = tmp->next;
    tmp->next = item->next;
    free( item );
}

process_t* find( process_t* root, int pid )
{
    while( root != nullptr )
    {
        if(root->pid == pid) break;
        root = root->next;
    }
    return root;
}

int traverse( process_t* root )
{
    int count = 0;
    while( root != nullptr )
    {
        root = root->next;
        count++;
    }
    return count;
}

process_t* read_file(char* filename)
{
    char line[256];
    process_t* root = nullptr;
    FILE* file = fopen( filename, "r" );
    if(!file)
    {
        fprintf(stderr, "ERROR - Unable to open file: %s\n", filename);
        return NULL;
    }
    while ( fgets(line, sizeof(line), file) )
    {
        char* ptr;
        int _pid, _quanta, _tickets;
        ptr = strtok( line, "," );
        _pid = atoi(ptr);
        ptr = strtok( nullptr, "," );
        _quanta = atoi(ptr);
        ptr = strtok( nullptr, "," );
        _tickets = atoi(ptr);
#ifdef DEBUG
        printf("line: %s, pid: %d, Q: %d, T:%d\n", line, _pid, _quanta, _tickets);
#endif
        root = insert( root, _pid, _quanta, _tickets );
    }
    fclose(file);
    return root;
}
