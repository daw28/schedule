/**
 * @file process_list.h
 * @author David Watson
 * @date 3 Aug 2017
 * @brief A header containing the definitions of data and methods used by the
 *        process list.
 *
 * A linked list of processes which each item containing processID, quanta and
 * number of lottery tickets as feilds. Each funtion accepts a pointer to the
 * root node of the list plus any other parameters the functions requires.
 * Standard linked list functions have been implemented: insert, delete,
 * traverse and find.
 */

/**
 * @breif a list item
 */
typedef struct process_st {
    int pid;    /** processID */
    int quanta; /** Quanta of time the process still needs to run (msec) */
    int tickets;/** Number of lottery tickets assigned to this process */
    struct process_st* next; /** pointer to next list item */
} process_t;

/**
 *  @breif Inserts a new process item at the head of the list
 *
 *  @param root Pointer to the head of the list
 *  @param _pid Process ID
 *  @param _quanta Quanta of CPU time to be used each timeslice
 *  @param _tickets Number of lottery tickets to determine scheduling order
 *  @return Returns pointer to the new item which is now the head of the list
 */
process_t* insert( process_t* root, int _pid, int _quanta, int _tickets);

/**
 *  @breif Deletes a Node from the list.
 *
 *  @param item Pointer to item to be deleted
 */
void remove_from_list( process_t** root, process_t* item );

/**
 *  @breif Returns a pointer to the item with the given pid
 *
 *  @param root Pointer to the head of the list
 *  @param pid process ID to be found
 *  @return Pointer to the item with the given pid or NULL if not found
 */
process_t* find( process_t* root, int pid );

/**
 *  @breif Traverses the linked list and returns the number of elements counted
 *
 *  @param root Pointer to the head of the list
 *  @return Number of elements in list
 */
int traverse( process_t* root );

/**
 *  @breif Creates a list of processes from a .csv file
 *
 *  @param filename Path to the csv file
 *  @return Pointer to the root item of the list
 */
process_t* read_file(char* filename);
